/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Fragment, Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import { firebase } from '@react-native-firebase/config';
import config from '@react-native-firebase/config';
// import analytics from '@react-native-firebase/analytics';
import functions from '@react-native-firebase/functions';
class App extends Component {

  // activateDebugMode = () => {
  //   let debugSettings = FIRRemoteConfigSettings(developerModeEnabled, true)
  //   FIRRemoteConfig.remoteConfig().configSettings !== debugSettings
  // }

  bootstrap = async() => {
    // await firebase.config().setDefaults({
    //   experiment_enabled: false,
    // });
    await firebase.config().setConfigSettings({
      isDeveloperModeEnabled: true,
    });
  }
  getValues = async() => {
    // const configValue = firebase.config().getValue('beta_enabled');
    // console.log('Value source: ', configValue.source);
    // console.log('Value source: ', configValue.value);
    // const values = firebase.config().getAll();
    // console.log('Value All: ', values);
    // firebase.config.ValueSource.STATIC;
    
    // const values = firebase.config().getAll();
 
    // Object.entries(values).forEach(($) => {
    //   const [key, entry] = $;
    //   console.log('Key: ', key);
    //   console.log('Source: ', entry.source);
    //   console.log('Value: ', entry.value);
    // });
    // const configValue = firebase.config().getValue('beta_enabled');
    // console.log('Source: ', configValue.source);
    // console.log('Value: ', configValue.value);
    // const activated = firebase.config().activate();
 
    // if (activated) {
    //   console.log('Fetched values successfully activated.');
    // } else {
    //   console.log('Fetched values failed to activate.');
    // }
    // const try1 = firebase.config.ValueSource;;
    // console.log(try1);
    
    
    try {
      
      const activated = firebase.config().fetchAndActivate();
   
      if (activated) {
        const experimentalFeatureEnabled = await config().getValue('experiment_enabled');
        console.log('Experimental source: ', experimentalFeatureEnabled.source);
        console.log('Experimental value: ', experimentalFeatureEnabled.value);
      }
    } catch (e) {
      console.error(e);
    }
    const success = await firebase.functions().httpsCallable( helloWorld => {
      alert("hello welcome");
      console.log("welcome");
    });
    console.log(success);
    if (__DEV__) {
      firebase.functions().useFunctionsEmulator('http://localhost:8082');
    }

    // // if (__DEV__) {
    // //   firebase.config().enableDeveloperMode();
    // // }
    
    // // Set default values
    // firebase.config().setDefaults({
    //   test_key: 'No value!'
    // });
    
    // firebase
    //   .config()
    //   .fetch(0)
    //   .then(() => firebase.config().activateFetched())
    //   .then(activated => {
    //     if (!activated) console.log('Fetched data not activated');
    //     return firebase.config().getValue('test_key');
    //   })
    //   .then(snapshot => {
    //     debugger
    //     console.log('LOG ==> ', snapshot.val()); // <== It always return my default value "No value!"
        
    //   })
    //   .catch(error => {
        
    //     console.error});
  }



  componentDidMount()
  {
    // this.activateDebugMode();
    this.bootstrap();
    this.getValues();
  }
  render() {
    return (
      <Fragment>
        <StatusBar barStyle="dark-content" />
        <SafeAreaView>
          <ScrollView
            contentInsetAdjustmentBehavior="automatic"
            style={styles.scrollView}>
            <Header />
            {global.HermesInternal == null ? null : (
              <View style={styles.engine}>
                <Text style={styles.footer}>Engine: Hermes</Text>
              </View>
            )}
            <View style={styles.body}>
              <View style={styles.sectionContainer}>
                <Text style={styles.sectionTitle}>Step One</Text>
                <Text style={styles.sectionDescription}>
                  Edit <Text style={styles.highlight}>App.js</Text> to change this
                  screen and then come back to see your edits.
              </Text>
              </View>
              <View style={styles.sectionContainer}>
                <Text style={styles.sectionTitle}>See Your Changes</Text>
                <Text style={styles.sectionDescription}>
                  <ReloadInstructions />
                </Text>
              </View>
              <View style={styles.sectionContainer}>
                <Text style={styles.sectionTitle}>Debug</Text>
                <Text style={styles.sectionDescription}>
                  <DebugInstructions />
                </Text>
              </View>
              <View style={styles.sectionContainer}>
                <Text style={styles.sectionTitle}>Learn More</Text>
                <Text style={styles.sectionDescription}>
                  Read the docs to discover what to do next:
              </Text>
              </View>
              <LearnMoreLinks />
            </View>
          </ScrollView>
        </SafeAreaView>
      </Fragment>
    );
  };
};
const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
